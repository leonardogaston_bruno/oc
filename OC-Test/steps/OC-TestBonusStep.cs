﻿using NUnit.Framework;
using OC_Test.pages;
using OC_Test.pages.PageObjects;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;

namespace OC_Test.steps
{
    [Binding]
    class OC_TestBonusStep
    {
        private static IWebDriver _driver = null;

        static AccountSelectionPage account;
        static PayPalMainPage page;
        static EmailPasswordPage emailPasswordPage;
        static AddressPage addressPage;
        static IntentSelectionPage intentSelection;
        [BeforeTestRun]
        public static void setup()
        {
            _driver = new ChromeDriver();
            page = new PayPalMainPage(_driver);
            account = new AccountSelectionPage(_driver);
            emailPasswordPage = new EmailPasswordPage(_driver);
            addressPage = new AddressPage(_driver);
            intentSelection = new IntentSelectionPage(_driver);
        }

        [AfterTestRun]
        public static void close()
        {
            _driver.Quit();
            _driver.Close();
        }

  
        [Given(@"The user has navigated to the form step one")]
        public void GivenTheUserHasNavigatedToTheFormStepOne()
        {
            page.goToUrl();
            page.clickOnSignUpButton();
            Thread.Sleep(500);
            account.clickNextButton();
        }

        [Given(@"The user has inserted the email")]
        public void GivenTheUserHasInsertedTheEmail()
        {
            
            emailPasswordPage.setEmail("test21052020@gmail.com");
            
        }

        [When(@"The user clicks on first name field")]
        public void WhenTheUserClicksOnFirstNameField()
        {
            
            emailPasswordPage.clickOnFirstName();
        }

        [Then(@"The systems has showed a help text")]
        public void ThenTheSystemsHasShowedAHelpText()
        {
            Assert.IsTrue(emailPasswordPage.isHelpTextDisplayed());
        }

        [Given(@"The user has inserted the password : (.*)")]
        public void GivenTheUserHasInsertedThePassword(string value)
        {
            emailPasswordPage.setPassword(value);
        }

        [Then(@"The systems has showed a red box error")]
        public void ThenTheSystemsHasShowedARedBoxError()
        {
            Assert.IsTrue(emailPasswordPage.isPasswordErrorRedBoxDisplayed());
            
        }

        [Given(@"The user has inserted the confirm password : (.*)")]
        public void GivenTheUserHasInsertedTheConfirmPassword(string value)
        {
            emailPasswordPage.setConfirmPassword(value);
        }

        [Given(@"The user has filled : (.*)")]
        public void GivenTheUserHasFilled(string value)
        {
            emailPasswordPage.fillsFormExceptOne(value);
        }

        [When(@"The user clicks next button")]
        public void WhenTheUserClicksNextButton()
        {
            emailPasswordPage.clickOnNextButton();
        }












    }
}
