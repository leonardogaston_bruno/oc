﻿using NUnit.Framework;
using OC_Test.pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using System;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using System.Threading.Tasks;
using OC_Test.pages.PageObjects;

namespace OC_Test.steps
{
    [Binding]
    public class PayPalMainPageSteps
    {

        private static IWebDriver _driver=null;

        static AccountSelectionPage account;
        static PayPalMainPage page;
        static EmailPasswordPage emailPasswordPage;
        static AddressPage addressPage;
        static IntentSelectionPage intentSelection;

        [BeforeTestRun]
        public static void setup()
        {
            _driver = new ChromeDriver();
            page = new PayPalMainPage(_driver);
            account = new AccountSelectionPage(_driver);
            emailPasswordPage = new EmailPasswordPage(_driver);
            addressPage = new AddressPage(_driver);
            intentSelection = new IntentSelectionPage(_driver);
        }

        [AfterTestRun]
        public static void close()
        {
            _driver.Quit();
            _driver.Close();
        }


        [Given(@"The user has navigated to PayPal page")]
        public void GivenTheUserHasNavigatedToPayPalPage()
        {
            page.goToUrl();
        }

         [Given(@"The user has clicked on Sign Up button")]
        public void GivenTheUserHasClickedOnSignUpButton()
        {
            page.clickOnSignUpButton();
        }

        [Given(@"The user has clicked on next button")]
        public void GivenTheUserHasClickedOnNextButton()
        {   
            account.clickNextButton();
        }

        [Given(@"The user has filled the form for step1")]
        public void GivenTheUserHasFilledTheFormForStep1()
        {
            String email = "test21052020@gmail.com";
            String firtName = "Leonardo";
            String lastName = "Bruno";
            String password = "test210520";
            String confirm = "test210520";
            emailPasswordPage.fillForm(email, firtName, lastName, password, confirm);
        }

        [Given(@"The user has filled the form for step2")]
        public void GivenTheUserHasFilledTheFormForStep2()
        {
            String address = "Calle test ";
            String address2 = "999";
            String city = "Waldorf";
            String zip = "20601";
            String phone = "2123244152";
            addressPage.fillForm(address, address2, city, zip, phone);
        }

        [When(@"The user clicks on create account button")]
        public void WhenTheUserClicksOnCreateAccountButton()
        {
            addressPage.agreeTerms();
        }

        [Then(@"The user has navigated to intent selection")]
        public void ThenTheUserHasNavigatedToIntentSelection()
        {
            Assert.AreEqual("https://www.sandbox.paypal.com/welcome/signup/#/intent_selection", intentSelection.getCurrentUrl());
        }




    }
}
