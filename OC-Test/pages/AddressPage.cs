﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium.Support.PageObjects;
using SeleniumExtras.PageObjects;
using OpenQA.Selenium.Support.UI;
using System.Threading;

namespace OC_Test.pages
{
    class AddressPage
    {
        public IWebElement addressInput => driver.FindElement(By.Id("paypalAccountData_addressSuggest"));
        public IWebElement address2Input => driver.FindElement(By.Id("paypalAccountData_address2"));
        public IWebElement cityInput => driver.FindElement(By.Id("paypalAccountData_city"));
        public IWebElement stateSelector => driver.FindElement(By.Id("paypalAccountData_state"));
        public IWebElement zipInput => driver.FindElement(By.Id("paypalAccountData_zip"));
        public IWebElement phoneSelector => driver.FindElement(By.Id("paypalAccountData_phoneType"));
        public IWebElement phoneInput => driver.FindElement(By.Id("paypalAccountData_phone"));
        public IWebElement checkBoxInput => driver.FindElement(By.Id("paypalAccountData_tcpa"));
        public IWebElement agreeButton => driver.FindElement(By.Id("/appData/action"));

        IWebDriver driver;

        public AddressPage(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        public void fillForm(String address, String address2, String city, String zip, String phone)
        {
            Thread.Sleep(2000);
            addressInput.SendKeys(address);
            address2Input.SendKeys(address2);
            cityInput.SendKeys(city);
            zipInput.SendKeys(zip);
            phoneInput.SendKeys(phone);
            this.selectState();
            this.selectPhone();
            checkBoxInput.Click();
        }

        public void selectState()
        {
            var selectElement = new SelectElement(stateSelector);
            selectElement.SelectByValue("MD");
        }

        public void selectPhone()
        {
            var selectElement = new SelectElement(phoneSelector);
            selectElement.SelectByValue("Mobile");
        }

        public void agreeTerms()
        {
            agreeButton.Click();
        }


    }
}
