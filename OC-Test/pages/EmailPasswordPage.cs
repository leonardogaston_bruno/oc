﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium.Support.PageObjects;
using System.Threading;

namespace OC_Test.pages
{


    class EmailPasswordPage
    {
        public IWebElement emailInput => driver.FindElement(By.Id("paypalAccountData_email"));
        public IWebElement firstNameInput => driver.FindElement(By.Id("paypalAccountData_firstName"));
        public IWebElement lastNameInput => driver.FindElement(By.Id("paypalAccountData_lastName"));
        public IWebElement passwordInput => driver.FindElement(By.Id("paypalAccountData_password"));
        public IWebElement confirmPasswordInput => driver.FindElement(By.Id("paypalAccountData_confirmPassword"));
        public IWebElement actionButton => driver.FindElement(By.Id("/appData/action"));
        public IWebElement helpText => driver.FindElement(By.Id("paypalAccountData_email_helptext"));

        public IWebElement passwordErrorBox => driver.FindElement(By.ClassName("vx_has-error-with-message"));

        IWebDriver driver;

        public EmailPasswordPage(IWebDriver driver)
        {

            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        public void fillForm(String email, String fName, String lName, String password, String cPassword)
        {
            this.setEmail(email);
            this.setFirstName(fName);
            this.setLastName(lName);
            this.setPassword(password);
            this.setConfirmPassword(cPassword);
            actionButton.Click();

        }

        public Boolean isHelpTextDisplayed()
        {
            return helpText.Displayed;
        }

        public void clickOnNextButton()
        {
            actionButton.Click();
        }

        public void setEmail(String value)
        {
            emailInput.SendKeys(value);
        }

        public void setFirstName(String value)
        {
            firstNameInput.SendKeys(value);
        }

        public void setLastName(String value)
        {
            lastNameInput.SendKeys(value);
        }
        public void setPassword(String value)
        {
            passwordInput.SendKeys(value);
        }
        public void setConfirmPassword(String value)
        {
            confirmPasswordInput.SendKeys(value);
        }

        public void clickOnFirstName()
        {
            firstNameInput.Click();
            Thread.Sleep(1500);
        }

       

        public Boolean isPasswordErrorRedBoxDisplayed()
        {
            return passwordErrorBox.Displayed;
        }

        

        public void fillsFormExceptOne(String value)
        {
            switch (value)
            {
                case "email":
                    this.setFirstName("Leonardo");
                    this.setLastName("Bruno");
                    this.setPassword("test210520");
                    this.setConfirmPassword("test210520");
                    break;
                case "firstName":
                    this.setEmail("test21052020@gmail.com");
                    this.setLastName("Bruno");
                    this.setPassword("test210520");
                    this.setConfirmPassword("test210520");
                    break;
                case "lastName":
                    this.setEmail("test21052020@gmail.com");
                    this.setFirstName("Leonardo");
                    this.setPassword("test210520");
                    this.setConfirmPassword("test210520");
                    break;
                case "password":
                    this.setEmail("test21052020@gmail.com");
                    this.setFirstName("Leonardo");
                    this.setLastName("test210520");
                    this.setConfirmPassword("test210520");
                    break;
                case "repeatPassword":
                    this.setEmail("test21052020@gmail.com");
                    this.setFirstName("Leonardo");
                    this.setLastName("test210520");
                    this.setPassword("test210520");
                    break;
            }
                

        }


    }
}
