﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Google.Protobuf.WellKnownTypes;
using NUnit.Framework;
using OC_Test.pages.PageObjects;
using OC_Test.steps;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using Selenium.Community.PageObjects;
using SeleniumExtras.PageObjects;


namespace OC_Test.pages
{
    class PayPalMainPage 
    {

        IWebDriver driver;
        public IWebElement signUpButton => driver.FindElement(By.Id("signup-button"));
        public IWebElement personalRadio => driver.FindElement(By.Id("radio-personal"));
        public IWebElement nextButton => driver.FindElement(By.Id("cta-btn"));


    public PayPalMainPage(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        public void goToUrl()
        {
            driver.Navigate().GoToUrl("https://www.sandbox.paypal.com");
            driver.Manage().Window.Maximize();
        }

        public void clickOnSignUpButton()
        {
            signUpButton.Click();  
        }

        public void clickNextButton()
        {
            nextButton.Click();
        }




    }
}
