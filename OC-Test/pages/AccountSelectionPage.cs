﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;
using OpenQA.Selenium.Support.PageObjects;
using SeleniumExtras.PageObjects;
using System.Threading;
using NUnit.Framework.Constraints;

namespace OC_Test.pages.PageObjects
{
    class AccountSelectionPage 
    {

        public IWebElement personalRadio => driver.FindElement(By.Id("radio-personal"));
        public IWebElement nextButton => driver.FindElement(By.Id("cta-btn"));
        IWebDriver driver;

        public AccountSelectionPage(IWebDriver driver)
        {
            
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }
        public void selectRadioPersonal()
        {
            personalRadio.Click();
        }
        public void clickNextButton()
        {
            
            nextButton.Click();
           
        }

    }
}
