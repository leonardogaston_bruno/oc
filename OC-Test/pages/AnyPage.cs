﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Text;
using TechTalk.SpecFlow;
using OpenQA.Selenium.Support.PageObjects;

namespace OC_Test.pages
{
    [Binding]
    public class AnyPage 
    {
        protected IWebDriver driver;

        
        public AnyPage() {
            
        }

        public void setDriver(IWebDriver driver)
        {
            this.driver = driver;
        }

        public IWebDriver getDriver()
        {
            return driver;
        }



       

        
    }
}
