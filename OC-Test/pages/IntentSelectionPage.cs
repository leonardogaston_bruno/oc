﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace OC_Test.pages
{
    class IntentSelectionPage
    {
        IWebDriver driver;

        public IntentSelectionPage(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        public String getCurrentUrl()
        {
            Thread.Sleep(5000);
            return driver.Url;
        }
    }
}
