﻿Feature: OCTestBonus
	

Scenario: The user inserts an exits email
The system shows a hel text if the user try to create an account with an email that already exist in the system
	Given The user has navigated to the form step one
	And The user has inserted the email
	When The user clicks on first name field
	Then The systems has showed a help text

Scenario: Check password lenght and consecutives number
The password should not be created if the lenght is less than 8 and has consecurtives numbers
	Given The user has navigated to the form step one
	And The user has inserted the password : <password>
	When The user clicks on first name field
	Then The systems has showed a red box error

	Examples: 
	| password  |
	| small     |
	| consecutives1234 |
	| 4287509854566 |

Scenario: Check confirm password field
This scenario test if password and confirm password are not equals. If not the system shows a red box error
	Given The user has navigated to the form step one
	And The user has inserted the password : <password>
	And The user has inserted the confirm password : <confirmation>
	When The user clicks on first name field
	Then The systems has showed a red box error

	Examples: 
	| password | confirmation |
	| test2020 | test2021     |

Scenario: Check mandatories fields in form step one
This scenario test if all field in form step one are mandotories. If not the system ahos a red box error
	Given The user has navigated to the form step one
	And The user has filled : <field>
	When The user clicks next button
	Then The systems has showed a red box error

	Examples:
	| field          |
	| email          |
	| firstName      |
	| lastName       |
	| password       |
	| repeatPassword |