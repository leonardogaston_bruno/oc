﻿Feature: OC-Test
The automated test must navigate from the base URL to the sign-up form, which will be submitted with valid data
in order to assert that the account has been successfully created.

Scenario: Creation of a personal account
	Given The user has navigated to PayPal page
	And The user has clicked on Sign Up button
	And The user has clicked on next button
	And The user has filled the form for step1
	And The user has filled the form for step2
	When The user clicks on create account button
	Then The user has navigated to intent selection



